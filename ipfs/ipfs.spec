%define name go-ipfs

Summary: Inter Planetary File System
Name: %{name}
Version: 0.6.0
Release: 1%{?dist}
Group: System Environment/Daemons
License: MIT/Apache2
URL: https://ipfs.io
Source0: https://github.com/ipfs/%{name}/releases/download/v%{version}/%{name}_v%{version}_linux-amd64.tar.gz
Source1: https://gitlab.com/hsn10/copr/-/raw/master/ipfs/ipfs.service
BuildRequires: coreutils

%description
Peer to peer hypermedia protocol

%global debug_package %{nil}

%prep
%setup -q -n %{name}

%build

%install
mkdir -p %{buildroot}%{_bindir}
cp -a ipfs %{buildroot}%{_bindir}
#unit file
install -d %{buildroot}/usr/lib/systemd/system
install -m644 -t %{buildroot}/usr/lib/systemd/system %{SOURCE1}

%files
%defattr(-, root, root)
%{_bindir}/ipfs
/usr/lib/systemd/system/ipfs.service

%post
systemctl daemon-reload

%preun
if [ $1 == 0 ]; then #uninstall
  systemctl unmask ipfs.service
  systemctl stop ipfs.service
  systemctl disable ipfs.service
fi

%postun
if [ $1 == 0 ]; then #uninstall
  systemctl daemon-reload
  systemctl reset-failed
fi

%changelog
* Sat May 09 2020 Radim Kolar <hsn@sendmail.cz>
- Initial packaging
