%define name fsp

Summary: File Service Protocol Clients
Name: %{name}
Version: 2.8.1b29
Release: 1%{?dist}
Group: System Environment/Daemons
License: BSD/MIT/X
URL: http://fsp.sourceforge.net
Source0: http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.bz2
BuildRequires: gcc glibc-devel python3-scons >= 3.0.0 flex

%description
FSP is a set of programs that implements a public-access
archive similar to an anonymous-FTP archive. It is not
meant to be a replacement for FTP; it is only meant to do
what anonymous-FTP does, but in a manner more acceptable
to the provider of the service and more friendly to the
clients.
FSP is UDP based and very useful on slow connections
and in a Wireless LAN.
This Package contains the fsp client programs.

%prep
%setup -q

%build
scons \
      without-clients=no \
      without-fspscan=yes \
      disable-timeout=no

%install
scons prefix=%{buildroot}%_prefix \
      sysconfdir=%_sysconfdir \
      mandir=%{buildroot}%_mandir \
      docdir=%{buildroot}/not-used \
      without-clients=no \
      without-fspscan=yes \
      disable-timeout=no \
      install

rm -rf %{buildroot}/not-used %{buildroot}%{_bindir}/fspmerge %{buildroot}%{_bindir}/fspd 

%files
%defattr(-, root, root)
%doc BETA.README COPYRIGHT INFO doc/PROTOCOL
%{_bindir}/fbye
%{_bindir}/fcatcmd
%{_bindir}/fcdcmd
%{_bindir}/fducmd
%{_bindir}/ffindcmd
%{_bindir}/fgetcmd
%{_bindir}/fgrabcmd
%{_bindir}/fhostcmd
%{_bindir}/flscmd
%{_bindir}/fmkdir
%{_bindir}/fmvcmd
%{_bindir}/fprocmd
%{_bindir}/fput
%{_bindir}/frmcmd
%{_bindir}/frmdircmd
%{_bindir}/fsetupcmd
%{_bindir}/fstatcmd
%{_bindir}/fver
%{_mandir}/man1/fbye.*
%{_mandir}/man1/fcat*
%{_mandir}/man1/fcd*
%{_mandir}/man1/fdu*
%{_mandir}/man1/ffind*
%{_mandir}/man1/fget*
%{_mandir}/man1/fgrab*
%{_mandir}/man1/fhost*
%{_mandir}/man1/fls*
%{_mandir}/man1/fmkdir.*
%{_mandir}/man1/fmv*
%{_mandir}/man1/fpro*
%{_mandir}/man1/fput.*


%changelog
* Sat Aug 24 2019 Radim Kolar <hsn@sendmail.cz>
- new upstream release 2.8.1b29
- tested on Centos 7

* Tue Jan 13 2004 Sven Hoexter <sven@wrack.telelev.de>
- new upstream release 2.8.1b19

* Tue Nov 18 2003 Sven Hoexter <sven@du-gehoerst-mir.de>
- new upstream release 2.8.1b17
- release should build with old flex versions
- fixed prob with condrestart in the postun scriptled
  it's not supported by the init script

* Sun Oct 26 2003 Sven Hoexter <sven@du-gehoerst-mir.de>
- changed the contents and names to the Debian naming scheme
- corrected my email address

* Sun Aug 24 2003 Sven Hoexter <sven@du-gehoerst-mir.de>
- Initial build.
