%define name  fspclient

Summary: File Service Protocol Client
Name: %{name}
Version: 0.93.1
Release: 3%{?dist}
Group: System Environment/Daemons
License: BSD/MIT/X
URL: http://fspclient.sourceforge.net
Source0: https://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.bz2
BuildRequires: gcc glibc-devel sed
%if 0%{?sle_version} >= 150100
BuildRequires: scons >= 3.0.0
%else
BuildRequires: python3-scons >= 3.0.0
%endif
%if 0%{?rhel} == 7
%define scons scons-3
%else
%define scons scons
%endif
%description
FSP client with FTP-like interface

%prep
%setup -q

%build
%{scons}

%install
%{scons} prefix=%{buildroot}%_prefix \
      sysconfdir=%_sysconfdir \
      mandir=%{buildroot}%_mandir \
      docdir=%{buildroot}/not-used \
      install
rm -rf %{buildroot}/not-used

%files
%defattr(-, root, root)
%doc doc/FOR.MORE.INFO README fsprc ChangeLog
%{_bindir}/fsp
%{_mandir}/man?/fsp.*

%changelog
* Fri May 22 2020 Radim Kolar <hsn@sendmail.cz> - 0.93.1-3
- Source0 linked to Source Forge download
- removed use of %%{buildroot} in %%build
- Switch to python3-scons
- Builds on Fedora 32 and EPEL 7

* Sat Aug 24 2019 Radim Kolar <hsn@sendmail.cz>
- Initial packaging
