%define name  fspd
%define sname fsp

Summary: File Service Protocol Daemon 
Name: %{name}
Version: 2.8.1b29
Release: 3%{?dist}
Group: System Environment/Daemons
License: BSD/MIT/X
URL: http://fsp.sourceforge.net
Source0: http://downloads.sourceforge.net/%{sname}/%{sname}-%{version}.tar.bz2
Source1: fspd.service
Source2: fspd.socket
BuildRequires: gcc glibc-devel flex
%if 0%{?sle_version} >= 150100
BuildRequires: scons >= 3.0.0
%else
BuildRequires: python3-scons >= 3.0.0
%endif
%if 0%{?rhel} == 7
%define scons scons-3
%else
%define scons scons
%endif

%description
FSP is a set of programs that implements a public-access
archive similar to an anonymous-FTP archive. It is not
meant to be a replacement for FTP; it is only meant to do
what anonymous-FTP does, but in a manner more acceptable
to the provider of the service and more friendly to the
clients.
FSP is UDP based and very useful on slow connections
and in a Wireless LAN.
This Package contains the fspd server program.

%prep
%setup -q -n %{sname}-%{version}

%build
%{scons} \
      without-clients=yes \
      without-fspscan=yes

%install
%{scons} prefix=%{buildroot}%_prefix \
      sysconfdir=%_sysconfdir \
      mandir=%{buildroot}%_mandir \
      docdir=%{buildroot}/not-used \
      without-clients=yes \
      without-fspscan=yes \
      install

rm -rf %{buildroot}/not-used

# config file
install -d %{buildroot}%{_sysconfdir}
install -m640 -t %{buildroot}%{_sysconfdir} fspd.conf

#systemd unit file
install -d %{buildroot}/usr/lib/systemd/system
install -m644 -t %{buildroot}/usr/lib/systemd/system %{SOURCE1} %{SOURCE2}

%post
systemctl daemon-reload

%preun
systemctl unmask fspd.service
systemctl stop fspd.service

%postun
systemctl daemon-reload
systemctl reset-failed

%files
%defattr(-, root, root)
%doc BETA.README ChangeLog fspd.conf COPYRIGHT INFO INSTALL TODO
%config(noreplace) %{_sysconfdir}/fspd.conf
%{_bindir}/fspd
%{_mandir}/man?/fspd.*
/usr/lib/systemd/system/fspd.service
/usr/lib/systemd/system/fspd.socket

%changelog
* Thu Jun 04 2020 Radim Kolar <hsn@sendmail.cz> - 2.8.1b29-3
- Add systemd unit file supports socket activation

* Sat Aug 24 2019 Radim Kolar <hsn@sendmail.cz>
- new upstream release 2.8.1b29
- tested on Centos 7

* Tue Jan 13 2004 Sven Hoexter <sven@wrack.telelev.de>
- new upstream release 2.8.1b19

* Tue Nov 18 2003 Sven Hoexter <sven@du-gehoerst-mir.de>
- new upstream release 2.8.1b17
- release should build with old flex versions
- fixed prob with condrestart in the postun scriptled
  it's not supported by the init script

* Sun Oct 26 2003 Sven Hoexter <sven@du-gehoerst-mir.de>
- changed the contents and names to the Debian naming scheme
- corrected my email address

* Sun Aug 24 2003 Sven Hoexter <sven@du-gehoerst-mir.de>
- Initial build.
