## COPR spec files

For yum repository with compiled binaries see:

https://copr.fedorainfracloud.org/coprs/hsn/

For development with rpkg set:

```
%_disable_source_fetch 0
```
in your ~/.rpmmacros and
```
[rpkg]
auto_pack = True
```
in your .config/rpkg.conf

## Flatpak manifests

Install flatpak-builder --install --user


